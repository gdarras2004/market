import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ProductList from './components/productList';
import ProductEdit from "./components/productEdit";
import CategoryEdit from "./components/categoryEdit";
import CategoryList from "./components/categoryList";
import SupplierEdit from "./components/supllierEdit";
import SupplierList from "./components/supplierList";

class App extends Component {

  render() {
    return (
        <Router>
          <Switch>
            <Route path='/' exact={true} component={Home}/>
            <Route path='/products' exact={true} component={ProductList}/>
            <Route path='/products/:id' component={ProductEdit}/>
            <Route path='/suppliers' exact={true} component={SupplierList}/>
            <Route path='/suppliers/:id' component={SupplierEdit}/>
            <Route path='/categories' exact={true} component={CategoryList}/>
            <Route path='/categories/:id' component={CategoryEdit}/>
          </Switch>
        </Router>

    )
  }
}

export default App;