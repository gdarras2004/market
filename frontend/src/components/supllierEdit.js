import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {Button, Container, Form, FormGroup, Input, Label} from 'reactstrap';
import AppNavbar from '../appNavBar';
import '../App.css';

class SupplierEdit extends Component {

    emptyItem = {
        afm: '',
        companyName: '',
        companyType: '',
        taxOffice: '',
        address: '',
        email: '',
        postCode: '',
        phone1: '',
        phone2: ''
    };


    constructor(props) {
        super(props);
        this.state = {
            item: this.emptyItem,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }


    async componentDidMount() {
        if (this.props.match.params.id !== 'new') {
            const supplier = await (await fetch(`/suppliers/${this.props.match.params.id}`)).json();
            this.setState({item: supplier});
        }
    }


    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item = {...this.state.item};
        item[name] = value;
        this.setState({item});
    }


    async handleSubmit(event) {
        event.preventDefault();
        const {item} = this.state;
        await fetch('/suppliers' + (item.id ? '/' + item.id : ''), {
            method: (item.id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item),
        });
        this.props.history.push('/suppliers');
    }


    render() {
        const {item} = this.state;
        const title = <h2>{item.id ? 'Επεξεργασία Προμηθευτή' : 'Προσθήκη Προμηθευτή'}</h2>;
        return <div>
            <AppNavbar/>
            <Container>
                {title}
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="afm">Α.Φ.Μ. Προμηθευτή</Label>
                        <Input type="text" name="afm" id="afm" value={item.afm || ''}
                               onChange={this.handleChange} autoComplete="afm"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="companyName">Επωνυμία Εταιρίας</Label>
                        <Input type="text" name="companyName" id="companyName" value={item.companyName || ''}
                               onChange={this.handleChange} autoComplete="companyName"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="companyType">Διακριτικός Τίτλος</Label>
                        <Input type="text" name="companyType" id="companyType" value={item.companyType || ''}
                               onChange={this.handleChange} autoComplete="companyType"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="taxOffice">Εφορία/Δ.Ο.Υ.</Label>
                        <Input type="text" name="taxOffice" id="taxOffice"
                               value={item.taxOffice || ''}
                               onChange={this.handleChange} autoComplete="taxOffice"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="address">Διέυθυνση</Label>
                        <Input type="text" name="address" id="address" value={item.address || ''}
                               onChange={this.handleChange} autoComplete="address"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="email">Email</Label>
                        <Input type="text" name="email" id="email" value={item.email || ''}
                               onChange={this.handleChange} autoComplete="email"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="postCode">Ταχυδρομικός κώδικας</Label>
                        <Input type="text" name="postCode" id="postCode" value={item.postCode || ''}
                               onChange={this.handleChange} autoComplete="postCode"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="phone1">Τηλέφωνο 1</Label>
                        <Input type="text" name="phone1" id="phone1" value={item.phone1 || ''}
                               onChange={this.handleChange} autoComplete="phone1"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="phone2">Τηλέφωνο 2</Label>
                        <Input type="text" name="phone2" id="phone2" value={item.phone2 || ''}
                               onChange={this.handleChange} autoComplete="phone2"/>
                    </FormGroup>

                    <FormGroup>
                        <Button color="primary" type="submit">Save</Button>{' '}
                        <Button color="secondary" tag={Link} to="/suppliers">Cancel</Button>
                    </FormGroup>
                </Form>
            </Container>
        </div>
    }
}

export default withRouter(SupplierEdit);