import React, { Component } from 'react';
import {Button, ButtonGroup, Container, Label, Table} from 'reactstrap';
import AppNavbar from '../appNavBar';
import { Link } from 'react-router-dom';
import '../App.css';

class SupplierList extends Component {

    constructor(props) {
        super(props);
        this.state = {suppliers: []};
        this.remove = this.remove.bind(this);
    }


    componentDidMount() {
        fetch('/suppliers/')
            .then(response => response.json())
            .then(data => this.setState({suppliers: data}));
    }

    async remove(id) {
        await fetch(`/suppliers/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updatedsuppliers = [...this.state.suppliers].filter(i => i.id !== id);
            this.setState({suppliers: updatedsuppliers});
        });
    }



    render() {
        const {suppliers} = this.state;
        const supplierList = suppliers.map(supplier => {
            return <tr key={supplier.id}>
                <td style={{whiteSpace: 'nowrap'}}>{supplier.afm}</td>
                <td>{supplier.companyName}</td>
                <td>{supplier.companyType}</td>
                <td>{supplier.taxOffice}</td>
                <td>{supplier.address}</td>
                <td>{supplier.email}</td>
                <td>{supplier.postCode}</td>
                <td>{supplier.phone1}</td>
                <td>{supplier.phone2}</td>
                <td>
                    <ButtonGroup>
                        <Button size="sm" color="primary" tag={Link} to={"/suppliers/" + supplier.id}>Επεξεργασία</Button>
                        <Button size="sm" color="danger" onClick={() => this.remove(supplier.id)}>Διαγραφή</Button>
                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <div className="float-sm-right">
                        <Button color="success" tag={Link} to="/suppliers/new">Προσθήκη νέου Προμηθευτή</Button>
                    </div>
                    <div className="input-group">
                        <input type="search" className="form-control rounded" placeholder="Search" aria-label="Search"
                               aria-describedby="search-addon" width={5} size={1} onresize={5}/>
                        <button type="button" className="btn btn-outline-primary">search</button>
                    </div>

                    <h3>suppliers</h3>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="10%">Α.Φ.Μ. Προμηθευτή</th>
                            <th width="30%">Επωνυμία Εταιρίας</th>
                            <th width="30%">Διακριτικός Τίτλος</th>
                            <th width="10%">Εφορία/Δ.Ο.Υ.</th>
                            <th width="10%">Διέυθυνση</th>
                            <th width="10%">Email</th>
                            <th width="10%">Ταχυδρομικός κώδικας</th>
                            <th width="10%">Τηλέφωνο 1</th>
                            <th width="10%">Τηλέφωνο 2</th>
                        </tr>
                        </thead>
                        <tbody>
                        {supplierList}
                        </tbody>
                    </Table>

                </Container>
            </div>
        );
    }
}

export default SupplierList;