import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {Button, Container, Form, FormGroup, Input, Label} from 'reactstrap';
import AppNavbar from '../appNavBar';
import '../App.css';

class ProductEdit extends Component {

    emptyItem = {
        quantity: '',
        descriptionGr: '',
        descriptionEn: '',
        supplierPrice: '',
        profitPercentage: '',
        suppliersAfm: '',
        barcode: '',
        categoryId: ''
    };

    emptyCategoryItem = {
        parentId: '',
        newCategoryName: ''
    }


    constructor(props) {
        super(props);
        this.state = {
            item: this.emptyItem,
            parentCategories: [],
            subCategories: [],
            subCategoryId: '',
            categoryItem: this.emptyCategoryItem,
            selectedSubCategoryDrop: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }


    async componentDidMount() {
        if (this.props.match.params.id !== 'new') {
            const product = await (await fetch(`/products/${this.props.match.params.id}`)).json();
            this.setState({item: product});
        }
        fetch('/categories')
            .then(response => response.json())
            .then(data => this.setState({parentCategories: data}));

    }


    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item = {...this.state.item};
        item[name] = value;
        this.setState({item});
    }

    handleChangeDropCat(event) {
        let item = {...this.state.item}
        item.parentId = (event.target.value);

        if (item.parentId === "-1") {
            this.setState({disabled: false});
        } else {
            this.setState({disabled: true});
            console.log("edooooo2 -> " + item.parentId)
            this.addSubCategory(item.parentId);

        }

        this.setState({item});
    }

    handleChangeDropSubCat(event) {
        let item = {...this.state.item}
        item.categoryId = (event.target.value);
        console.log("======>" + item.categoryId)
        this.setState({item});
    }

    async handleSubmit(event) {
        event.preventDefault();
        const {item} = this.state;
        console.log("======> BEFORE SUBMIT " + item.categoryId)
        await fetch('/products' + (item.id ? '/' + item.id : ''), {
            method: (item.id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item),
        });
        this.props.history.push('/products');
    }

    async addSubCategory(id) {
        fetch(`/categories/${id}`)
            .then(response => response.json())
            .then(data => this.setState({subCategories: data}));
    }

    render() {
        const {item} = this.state;
        const title = <h2>{item.id ? 'Edit product' : 'Add product'}</h2>;
        const categoryOptions = this.state.parentCategories.map(option =>
            <option key={option.categoryId} value={option.categoryId}>{option.categoryName}</option>
        )
        const SubCategoryOptions = this.state.subCategories.map(option =>
            <option key={option.categoryId} value={option.categoryId}>{option.categoryName}</option>
        )
        return <div>
            <AppNavbar/>
            <Container>
                {title}
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="descriptionGr">Περιγραφή GR</Label>
                        <Input type="text" name="descriptionGr" id="descriptionGr" value={item.descriptionGr || ''}
                               onChange={this.handleChange} autoComplete="descriptionGr"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="descriptionEn">Περιγραφή EN</Label>
                        <Input type="text" name="descriptionEn" id="descriptionEn" value={item.descriptionEn || ''}
                               onChange={this.handleChange} autoComplete="descriptionEn"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="supplierPrice">Καθαρή Τιμή Προμηθευτή</Label>
                        <Input type="text" name="supplierPrice" id="supplierPrice" value={item.supplierPrice || ''}
                               onChange={this.handleChange} autoComplete="supplierPrice"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="profitPercentage">Ποσοστό κέρδους(%) π.χ. 30</Label>
                        <Input type="text" name="profitPercentage" id="profitPercentage"
                               value={item.profitPercentage || ''}
                               onChange={this.handleChange} autoComplete="profitPercentage"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="suppliersAfm">ΑΦΜ Προμηθευτή</Label>
                        <Input type="text" name="suppliersAfm" id="suppliersAfm" value={item.suppliersAfm || ''}
                               onChange={this.handleChange} autoComplete="suppliersAfm"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="quantity">Ποσότητα</Label>
                        <Input type="text" name="quantity" id="quantity" value={item.quantity || ''}
                               onChange={this.handleChange} autoComplete="quantity"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="barcode">Barcode</Label>
                        <Input type="text" name="barcode" id="barcode" value={item.barcode || ''}
                               onChange={this.handleChange} autoComplete="barcode"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="choose_category">Επιλογή κατηγορίας</Label>
                        <select
                            className="form-control"
                            name={this.state.selectedDrop}
                            onChange={this.handleChangeDropCat.bind(this)}>
                            <option value="-1">Επιλογή Κατηγορίας</option>
                            {categoryOptions}
                        </select>
                    </FormGroup>

                    <FormGroup>
                        <Label for="choose_sub_category">Επιλογή κατηγορίας</Label>
                        <select
                            className="form-control"
                            name={this.state.selectedSubCategoryDrop}
                            onChange={this.handleChangeDropSubCat.bind(this)}
                            disabled={!this.state.disabled}>
                            <option value="">Επιλογή Υπόκατηγοριας</option>
                            {SubCategoryOptions}
                        </select>
                    </FormGroup>


                    <FormGroup>
                        <Button color="primary" type="submit">Save</Button>{' '}
                        <Button color="secondary" tag={Link} to="/products">Cancel</Button>
                    </FormGroup>
                </Form>
            </Container>
        </div>
    }
}

export default withRouter(ProductEdit);