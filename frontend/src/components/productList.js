import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from '../appNavBar';
import { Link } from 'react-router-dom';

class ProductList extends Component {

    constructor(props) {
        super(props);
        this.state = {products: [], checkedItems: new Map()};
        this.remove = this.remove.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    componentDidMount() {
        fetch('/products')
            .then(response => response.json())
            .then(data => this.setState({products: data}));
    }

    async remove(id) {
        await fetch(`/products/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updatedProducts = [...this.state.products].filter(i => i.id !== id);
            this.setState({products: updatedProducts});
        });
    }

    async printProductLabel(id) {
        await fetch(`/products/${id}/label`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
    }

    async printProductLabelFromAList(ids) {
        await fetch(`/products/label`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ productIds: ids })
        });
    }

    handleSubmit(event) {
        console.log(this.state);
        let ids = Array.from(this.state.checkedItems.keys())
        event.preventDefault();
        console.log(`==========> :: ${ids}`);
        this.printProductLabelFromAList(ids);


    }

    handleChange(event) {
        var isChecked = event.target.checked;
        var item = event.target.value;
        console.log(`==========> :: ${item}`);
        this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(item, isChecked) }));
    }


    render() {
        const {products} = this.state;
        const productList = products.map(product => {
            return <tr key={product.id}>
                <input
                    type="checkbox"
                    value={product.id}
                    onChange={this.handleChange}
                /> {product.value}
                <td style={{whiteSpace: 'nowrap'}}>{product.descriptionGr}</td>
                <td>{product.descriptionEn}</td>
                <td>{product.salesPricePerUnit}</td>
                <td>{product.supplierPricePerUnit}</td>
                <td>{product.profitPercentage + " %"}</td>
                <td>{product.quantity}</td>
                <td>
                    <ButtonGroup>
                        <Button size="sm" color="primary" tag={Link} to={"/products/" + product.id}>Επεξεργασία</Button>
                        <Button size="sm" color="warning" onClick={() => this.printProductLabel(product.id)}>Ετικέτα</Button>
                        <Button size="sm" color="danger" onClick={() => this.remove(product.id)}>Διαγραφή</Button>
                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <div className="float-sm-right">
                        <Button color="primary" tag={Link} to="/categories/new-category">Προσθήκη νέας κατηγορίας</Button>
                         &nbsp;&nbsp;&nbsp;
                        <Button color="success" tag={Link} to="/products/new">Προσθήκη νέου είδους</Button>
                         &nbsp;&nbsp;&nbsp;
                        <Button color="warning" onClick={this.handleSubmit}>Δημιουργία πολλαπλών ετικετών </Button>
                    </div>

                    <h3>products</h3>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="10%">Επιλογή είδους</th>
                            <th width="30%">Περιγραφή GR</th>
                            <th width="30%">Περιγραφή En</th>
                            <th width="10%">Τιμή πώλησης</th>
                            <th width="10%">Τιμή Προμηθευτή</th>
                            <th width="10%">Ποσοστό κέρδους</th>
                            <th width="10%">Ποσότητα</th>
                            <th width="40%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {productList}
                        </tbody>
                    </Table>

                </Container>
            </div>
        );
    }
}

export default ProductList;