import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from '../appNavBar';
import '../App.css';

class CategoryEdit extends Component {

  emptyItem = {
    parentId: '',
    newCategoryName: ''
  };

  constructor(props) {
    super(props);
    this.state = {item: this.emptyItem, parentCategories: [], selectedDrop:''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    fetch('/categories')
    .then(response => response.json())
    .then(data => this.setState({parentCategories: data}));
  }



  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let item = {...this.state.item};
    item[name] = value;
    console.log("malakies" + value);
    this.setState({item});
  }

  handleChangeDrop(event) {
    let item = {...this.state.item}
    item.parentId = (event.target.value);
    this.setState({item});
  }


  async handleSubmit(event) {
    event.preventDefault();
    const {item} = this.state;

    await fetch('/categories' + (item.id ? '/' + item.id : ''), {
      method: (item.id) ? 'PUT' : 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item),
    });
    this.props.history.push('/categories');
  }

  render() {
    const {item} = this.state;
    const title = <h2>{item.id ? 'Edit product' : 'Add product'}</h2>;
    const options = this.state.parentCategories.map(option =>
        <option key={option.categoryId} value={option.categoryId}>{option.categoryName}</option>
    )
    return <div>
      <AppNavbar/>
      <Container>
        {title}
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="choose_category">Επιλογή κατηγορίας</Label>
            <select
                className="form-control"
                name={this.state.selectedDrop}
                onChange={this.handleChangeDrop.bind(this)}>
              <option value="">Επιλογή Κατηγορίας για τη δημιουργία υποκατηγορίας</option>
              {options}
            </select>
          </FormGroup>

          <FormGroup>
            <Label for="newCategoryName">Περιγραφή GR</Label>

            <Input type="text" name="newCategoryName" id="newCategoryName" value={item.newCategoryName || ''}
                   onChange={this.handleChange} autoComplete="newCategoryName"/>

          </FormGroup>
          <FormGroup>
            <Button color="primary" type="submit">Save</Button>{' '}
            <Button color="secondary" tag={Link} to="/products">Cancel</Button>
          </FormGroup>
        </Form>
      </Container>
    </div>
  }
}

export default withRouter(CategoryEdit);

