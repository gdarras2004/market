package gr.darras.marketcafe.sales.controller.supplier;

import gr.darras.marketcafe.sales.business.service.supplier.BusinessSupplier;
import gr.darras.marketcafe.sales.model.supplier.CreateSupplierRequestDto;
import gr.darras.marketcafe.sales.model.supplier.SupplierDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/suppliers")
public class SupplierController {

    private final BusinessSupplier businessSupplier;

    public SupplierController(BusinessSupplier businessSupplier) {
        this.businessSupplier = businessSupplier;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<SupplierDto> createSupplier(@Valid
                                                      @RequestBody CreateSupplierRequestDto requestDto) {
        return ResponseEntity
                .ok(businessSupplier.createSupplier(requestDto));
    }

    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<SupplierDto> updateSupplier(@Valid @RequestBody CreateSupplierRequestDto requestDto,
                                                      @PathVariable("id") Long id) {
        return ResponseEntity
                .ok(businessSupplier.updateSupplier(id, requestDto));
    }

    @DeleteMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Boolean> deleteSupplier(@PathVariable("id") Long id) {
        return ResponseEntity
                .ok(businessSupplier.deleteSupplier(id));
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<SupplierDto>> getSuppliers() {
        return ResponseEntity
                .ok(businessSupplier.getSuppliers());
    }

    @GetMapping({"/{search}"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<SupplierDto>> getSuppliersByDescription(@PathVariable("search") String search) {
        return ResponseEntity
                .ok(businessSupplier.getSuppliersByDescription(search));
    }
}
