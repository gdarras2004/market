package gr.darras.marketcafe.sales.controller.product;

import gr.darras.marketcafe.sales.business.service.product.BusinessProductService;
import gr.darras.marketcafe.sales.model.product.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final BusinessProductService businessProductService;

    public ProductController(BusinessProductService businessProductService) {
        this.businessProductService = businessProductService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ProductDto> create(@Valid
            @RequestBody CreateProductDtoRequest requestDto) {
        return ResponseEntity
                .ok(businessProductService.createProduct(requestDto));
    }

    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ProductDto> editProductById(
            @PathVariable("id") Long productId,
            @RequestBody CreateProductDtoRequest requestDto) {
        return ResponseEntity
                .ok(businessProductService.editProductById(requestDto, productId));
    }

    @DeleteMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Boolean> deleteProductById(
            @PathVariable("id") Long productId) {
        return ResponseEntity
                .ok(businessProductService.deleteProductById(productId));
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductDtoForList>> getAllProducts() {
        return ResponseEntity
                .ok(businessProductService.getAllProducts());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ProductDto> getProductById(
            @PathVariable("id") Long productId) {
        return ResponseEntity
                .ok(businessProductService.getProductById(productId));
    }

    @PostMapping("/{id}/label")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ProductDto> createLabelByProductId(
            @PathVariable("id") Long productId) {
        return ResponseEntity
                .ok(businessProductService.createLabelByProductId(productId));
    }

    @PostMapping("/label")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductDtoForList>> createLabelsByListOfProductIds(
            @RequestBody CreateLabelsByListOfProductIds createLabelsByListOfProductIds) {
        return ResponseEntity
                .ok(businessProductService.createLabelsByListOfProductIds(createLabelsByListOfProductIds));
    }

}
