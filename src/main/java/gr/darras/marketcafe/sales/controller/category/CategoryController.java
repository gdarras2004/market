package gr.darras.marketcafe.sales.controller.category;

import gr.darras.marketcafe.sales.business.service.category.BusinessCategoryService;
import gr.darras.marketcafe.sales.model.category.*;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/categories")
public class CategoryController {

  private final BusinessCategoryService businessCategoryService;

  public CategoryController(
      BusinessCategoryService businessCategoryService) {
    this.businessCategoryService = businessCategoryService;
  }


  @PostMapping()
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<CategoryDtoAllResponse> createCategory(
      @RequestBody @Valid CreateCategoryRequestDto createCategoryRequestDto) {
    return ResponseEntity
        .ok(businessCategoryService.saveCategory(createCategoryRequestDto));
  }


  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<CategoryDtoParentResponse>> getAllParentCategories() {
    return ResponseEntity
            .ok(businessCategoryService.getAllParentCategories());
  }


  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<CategoryDtoAllResponse>> getAllSubCategories(
          @PathVariable("id") Long id) {
    return ResponseEntity
            .ok(businessCategoryService.getAllSubCategories(id));
  }

}
