package gr.darras.marketcafe.sales.error;

public enum LabelError implements ApplicationError{

    CANNOT_CREATE_LABEL(2001, "Cannot create Labels");

    private int code;

    private String message;

    LabelError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getType() {
        return getClass().getSimpleName();
    }

    @Override
    public String getMessage() {
        return message;
    }
}
