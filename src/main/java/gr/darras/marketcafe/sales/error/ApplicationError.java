package gr.darras.marketcafe.sales.error;

public interface ApplicationError {

    int getCode();

    String getType();

    String getMessage();

    default String getFullErrorDetails() {
        return String.format("%s : %s", getCode(), getMessage());
    }
}
