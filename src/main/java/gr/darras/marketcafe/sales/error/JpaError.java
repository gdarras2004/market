package gr.darras.marketcafe.sales.error;

public enum JpaError implements ApplicationError{

    ENTITY_NOT_FOUND(1001, "Entity not found");

    private int code;

    private String message;

    JpaError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getType() {
        return getClass().getSimpleName();
    }

    @Override
    public String getMessage() {
        return message;
    }
}
