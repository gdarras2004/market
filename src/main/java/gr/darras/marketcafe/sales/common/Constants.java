package gr.darras.marketcafe.sales.common;

import java.math.BigDecimal;

public class Constants {

    public static final BigDecimal FPA= new BigDecimal(1.24);
    public static final String BASE_PATH = "C:/Softwares/Market/";
}
