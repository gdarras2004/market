package gr.darras.marketcafe.sales.exception;

import gr.darras.marketcafe.sales.error.ApplicationError;

public class JpaException extends ApplicationException{

    public JpaException(ApplicationError error) {
        super(error);
    }
}
