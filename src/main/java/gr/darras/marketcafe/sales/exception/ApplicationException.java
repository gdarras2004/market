package gr.darras.marketcafe.sales.exception;

import gr.darras.marketcafe.sales.error.ApplicationError;

public class ApplicationException extends RuntimeException{

    private static final long serialVersionUID = 2775319336937706720L;

    private final ApplicationError error;

    public ApplicationException(ApplicationError error) {
        this.error = error;
    }

    public ApplicationException(ApplicationError error, Throwable e) {
        super(e);
        this.error = error;
    }

}
