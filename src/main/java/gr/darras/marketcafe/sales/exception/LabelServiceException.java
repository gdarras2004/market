package gr.darras.marketcafe.sales.exception;

import gr.darras.marketcafe.sales.error.ApplicationError;

public class LabelServiceException extends ApplicationException{

    public LabelServiceException(ApplicationError error) {
        super(error);
    }
}
