package gr.darras.marketcafe.sales.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Data
@Builder
@Component
@NoArgsConstructor
@AllArgsConstructor
public class CreateProductDtoResponse {

    private Long quantity;
    private String descriptionGr;
    private String descriptionEn;
    private BigDecimal salesPricePerUnit;
    private String barcode;

}
