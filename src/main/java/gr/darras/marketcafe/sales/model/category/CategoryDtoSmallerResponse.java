package gr.darras.marketcafe.sales.model.category;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CategoryDtoSmallerResponse {

    private Long categoryId;
    private String categoryName;
    private String categoryParentName;
    private String categoryParentId;
    private Set<String> childrenNames;
    private String productName;
    private Long productId;

}
