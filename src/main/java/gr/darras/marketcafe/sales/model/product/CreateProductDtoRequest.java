package gr.darras.marketcafe.sales.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.*;
import java.math.BigDecimal;

@Data
@Builder
@Component
@NoArgsConstructor
@AllArgsConstructor
public class CreateProductDtoRequest {

    @NotNull
    @Min(1)
    private Long quantity;

    @NotNull
    @NotBlank(message = "cannot be empty")
    @NotEmpty(message = "Please provide a name")
    @Size(min = 5, max = 255)
    private String descriptionGr;

    @NotNull
    @NotBlank(message = "cannot be empty")
    @NotEmpty(message = "Please provide a name")
    @Size(min = 5, max = 255)
    private String descriptionEn;

    @Min(0)
    private BigDecimal supplierPrice;

    @Min(5)
    private BigDecimal profitPercentage;

    @NotNull
    @NotBlank(message = "cannot be empty")
    @NotEmpty(message = "Please provide a name")
    @Size(min = 9, max = 9)
    private String suppliersAfm;

    @NotNull
    @NotBlank(message = "cannot be empty")
    @NotEmpty(message = "Please provide a name")
    private String barcode;

    @NotNull
    @Min(1)
    private Long categoryId;


}
