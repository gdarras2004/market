package gr.darras.marketcafe.sales.model.product;

import gr.darras.marketcafe.sales.model.BarcodeDto;
import gr.darras.marketcafe.sales.model.category.CategoryDtoResponse;
import gr.darras.marketcafe.sales.model.supplier.SupplierDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDto {

    private Long id;
    private Long quantity;
    private String descriptionGr;
    private String descriptionEn;
    private BigDecimal supplierPricePerUnit;
    private BigDecimal salesPricePerUnit;
    private BigDecimal profitPercentage;
    private OffsetDateTime creationDate;
    private OffsetDateTime lastUpdated;
    private CategoryDtoResponse categoryDtoResponse;
    private List<SupplierDto> supplierDtos;
    private List<BarcodeDto> barcodeDtos;
}
