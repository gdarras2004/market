package gr.darras.marketcafe.sales.model.supplier;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SupplierDto {

    private Long id;
    private Long afm;
    private String companyName;
    private String companyType;
    private String taxOffice;
    private String address;
    private String email;
    private Long postCode;
    private Long phone1;
    private Long phone2;
    private OffsetDateTime creationDate;
    private OffsetDateTime lastUpdated;

}
