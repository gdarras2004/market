package gr.darras.marketcafe.sales.model.category;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gr.darras.marketcafe.sales.jpa.entity.Category;
import gr.darras.marketcafe.sales.jpa.entity.Product;
import java.util.Set;

import gr.darras.marketcafe.sales.model.product.ProductDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties({"children"})
public class CategoryDtoResponse {

  private Long Id;
  private String name;
  private CategoryDtoResponse parent;
  private Set<CategoryDtoResponse> children;
  private ProductDto product;


}
