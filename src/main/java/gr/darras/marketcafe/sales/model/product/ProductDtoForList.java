package gr.darras.marketcafe.sales.model.product;

import gr.darras.marketcafe.sales.model.BarcodeDto;
import gr.darras.marketcafe.sales.model.supplier.SupplierDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDtoForList {

    private Long id;
    private Long quantity;
    private String descriptionGr;
    private String descriptionEn;
    private BigDecimal salesPricePerUnit;
    private BigDecimal supplierPricePerUnit;
    private BigDecimal profitPercentage;
}
