package gr.darras.marketcafe.sales.model.category;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CategoryDtoAllResponse {

    private Long categoryId;
    private String categoryName;
    private String categoryParentName;
    private Set<Long> productIds;

}
