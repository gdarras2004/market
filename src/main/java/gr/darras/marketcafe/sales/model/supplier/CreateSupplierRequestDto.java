package gr.darras.marketcafe.sales.model.supplier;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.*;
import java.time.OffsetDateTime;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateSupplierRequestDto {

    @NotNull
    @Size(min = 8, max = 15)
    private String afm;

    @NotNull
    @NotBlank(message = "cannot be empty")
    @NotEmpty(message = "Please provide a name")
    @Size(min = 3, max = 50)
    private String companyName;

    @NotNull
    @NotBlank(message = "cannot be empty")
    @NotEmpty(message = "Please provide a name")
    @Size(min = 3, max = 50)
    private String companyType;

    @NotNull
    @NotBlank(message = "cannot be empty")
    @NotEmpty(message = "Please provide a name")
    @Size(min = 3, max = 50)
    private String taxOffice;

    @NotNull
    @NotBlank(message = "cannot be empty")
    @NotEmpty(message = "Please provide a name")
    @Size(min = 3, max = 255)
    private String address;

    @Email
    private String email;

    @NotNull
    private Long postCode;

    @NotNull
    private Long phone1;
    private Long phone2;

}
