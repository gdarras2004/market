package gr.darras.marketcafe.sales.model.category;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateCategoryRequestDto {

    @NotNull
    private String parentId;

    @NotNull
    @NotBlank(message = "cannot be empty")
    @NotEmpty(message = "Please provide a name")
    @Size(min = 3, max = 128)
    private String newCategoryName;

}
