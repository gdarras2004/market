package gr.darras.marketcafe.sales.model.category;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CategoryDtoParentResponse {

  private Long categoryId;
  private String categoryName;
  private Long parentCategoryId;
  private String parentCategoryName;

}
