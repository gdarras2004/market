package gr.darras.marketcafe.sales.business.service.category;

import gr.darras.marketcafe.sales.jpa.entity.Category;
import gr.darras.marketcafe.sales.jpa.service.category.CategoryService;
import gr.darras.marketcafe.sales.mapper.CategoryCustomMapper;
import gr.darras.marketcafe.sales.model.category.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BusinessCategoryServiceImpl implements BusinessCategoryService {

  private final CategoryService categoryService;
  private final CategoryCustomMapper categoryCustomMapper;

  @Autowired
  public BusinessCategoryServiceImpl(
          CategoryService categoryService,
          CategoryCustomMapper categoryCustomMapper) {
    this.categoryService = categoryService;
    this.categoryCustomMapper = categoryCustomMapper;
  }

  @Override
  public CategoryDtoAllResponse saveCategory(CreateCategoryRequestDto createCategoryRequestDto) {
    if (!createCategoryRequestDto.getParentId().isEmpty() || !createCategoryRequestDto
        .getParentId().isBlank()) {
      Category parentCategory = categoryService
          .findCategoryById(Long.valueOf(createCategoryRequestDto.getParentId()));

      Category categoryChild = categoryBuilder(createCategoryRequestDto.getNewCategoryName());
      categoryChild.setName(createCategoryRequestDto.getNewCategoryName().toUpperCase());
      categoryChild.setParent(parentCategory);
      parentCategory.addChild(categoryChild);
      categoryService.saveCategory(categoryChild);

      return categoryCustomMapper.entityToModel(categoryService.saveCategory(parentCategory));
    }
    Category parentCategory = categoryService
        .saveCategory(categoryBuilder(createCategoryRequestDto.getNewCategoryName()));
    return categoryCustomMapper.entityToModel(parentCategory);
  }

  @Override
  public List<CategoryDtoParentResponse> getAllParentCategories() {
    return categoryCustomMapper.entityListToModelParent(categoryService.getAllParentCategories());
  }

  @Override
  public List<CategoryDtoAllResponse> getAllSubCategories(Long id) {
    return categoryCustomMapper.entityListToModel(categoryService.getAllSubCategories(id));
  }

  private Category categoryBuilder(String name) {
    return new Category().builder()
        .name(name.toUpperCase())
        .build();
  }



}


