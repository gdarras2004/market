package gr.darras.marketcafe.sales.business.service.barcode;

import gr.darras.marketcafe.sales.jpa.entity.Barcode;
import gr.darras.marketcafe.sales.jpa.repository.BarcodeRepository;
import org.springframework.stereotype.Service;

@Service
public class BusinessBarcodeServiceImpl implements BusinessBarcodeService {

    private final BarcodeRepository barcodeRepository;

    public BusinessBarcodeServiceImpl(BarcodeRepository barcodeRepository) {
        this.barcodeRepository = barcodeRepository;
    }

    @Override
    public Barcode createBarcode(Barcode barcode) {
        return barcodeRepository.save(barcode);
    }
}
