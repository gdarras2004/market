package gr.darras.marketcafe.sales.business.service.supplier;

import gr.darras.marketcafe.sales.model.supplier.CreateSupplierRequestDto;
import gr.darras.marketcafe.sales.model.supplier.SupplierDto;

import java.util.List;

public interface BusinessSupplier {

    SupplierDto createSupplier(CreateSupplierRequestDto createSupplierRequestDto);

    SupplierDto updateSupplier(Long id, CreateSupplierRequestDto createSupplierRequestDto);

    Boolean deleteSupplier(Long id);

    List<SupplierDto> getSuppliers();

    List<SupplierDto> getSuppliersByDescription(String search);

    SupplierDto findSupplierByAfm(Long afm);
}
