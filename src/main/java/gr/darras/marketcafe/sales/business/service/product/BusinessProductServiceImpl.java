package gr.darras.marketcafe.sales.business.service.product;

import com.itextpdf.text.DocumentException;
import gr.darras.marketcafe.sales.common.Constants;
import gr.darras.marketcafe.sales.error.LabelError;
import gr.darras.marketcafe.sales.exception.LabelServiceException;
import gr.darras.marketcafe.sales.jpa.entity.Barcode;
import gr.darras.marketcafe.sales.jpa.entity.Category;
import gr.darras.marketcafe.sales.jpa.entity.Product;
import gr.darras.marketcafe.sales.jpa.entity.Supplier;
import gr.darras.marketcafe.sales.jpa.service.barcode.BarcodeService;
import gr.darras.marketcafe.sales.jpa.service.category.CategoryService;
import gr.darras.marketcafe.sales.jpa.service.pdf.LabelService;
import gr.darras.marketcafe.sales.jpa.service.product.ProductService;
import gr.darras.marketcafe.sales.jpa.service.supplier.SupplierService;
import gr.darras.marketcafe.sales.mapper.ProductMapper;
import gr.darras.marketcafe.sales.model.product.CreateLabelsByListOfProductIds;
import gr.darras.marketcafe.sales.model.product.CreateProductDtoRequest;
import gr.darras.marketcafe.sales.model.product.ProductDto;
import gr.darras.marketcafe.sales.model.product.ProductDtoForList;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BusinessProductServiceImpl implements BusinessProductService {

  private final SupplierService supplierService;
  private final ProductService productService;
  private final ProductMapper productMapper;
  private final BarcodeService barcodeService;
  private final LabelService labelService;
  private final CategoryService categoryService;


  @Autowired
  public BusinessProductServiceImpl(SupplierService supplierService,
      ProductService productService, ProductMapper productMapper, BarcodeService barcodeService,
      LabelService labelService, CategoryService categoryService) {
    this.supplierService = supplierService;
    this.productService = productService;
    this.productMapper = productMapper;
    this.barcodeService = barcodeService;
    this.labelService = labelService;
    this.categoryService = categoryService;

  }

  @Override
  public ProductDto createProduct(CreateProductDtoRequest createProductDtoRequest) {
    Supplier supplier = supplierService.getSupplierByAfm(createProductDtoRequest.getSuppliersAfm());
    Product product = Product.builder()
        .creationDate(OffsetDateTime.now())
        .lastUpdated(OffsetDateTime.now())
        .descriptionGr(createProductDtoRequest.getDescriptionGr().toUpperCase())
        .descriptionEn(createProductDtoRequest.getDescriptionEn().toUpperCase())
        .quantity(createProductDtoRequest.getQuantity())
        .profitPercentage(createProductDtoRequest.getProfitPercentage())
        .supplierPricePerUnit(createProductDtoRequest.getSupplierPrice())
        .salesPricePerUnit(calculateSalesPrice(createProductDtoRequest.getSupplierPrice(),
            createProductDtoRequest.getProfitPercentage()))
        .build();

    Category category = createCategoryRequestDtoBuilder(createProductDtoRequest.getCategoryId());

    product.setCategory(category);
    product.getSuppliers().add(supplier);
    Barcode newBarcode = barcodeBuilder(createProductDtoRequest.getBarcode());
    newBarcode.setProductBarcode(product);
    barcodeService.createBarcode(newBarcode);
    productService.saveProduct(product);
    category.addProduct(product);
    categoryService.saveCategory(category);
    return productMapper.productToProductDto(product);
  }

  @Override
  public ProductDto editProductById(CreateProductDtoRequest createProductDtoRequest,
      Long productId) {
    Product product = productService.findProductById(productId);
    if (createProductDtoRequest.getDescriptionEn() != null) {
      product.setDescriptionEn(createProductDtoRequest.getDescriptionEn());
    }
    if (createProductDtoRequest.getDescriptionGr() != null) {
      product.setDescriptionGr(createProductDtoRequest.getDescriptionGr());
    }
    if (createProductDtoRequest.getProfitPercentage() != null) {
      product.setProfitPercentage(createProductDtoRequest.getProfitPercentage());
    }
    if (createProductDtoRequest.getQuantity() != null) {
      product.setQuantity(createProductDtoRequest.getQuantity());
    }
    if (createProductDtoRequest.getSupplierPrice() != null) {
      product.setSupplierPricePerUnit(createProductDtoRequest.getSupplierPrice());
      product.setSalesPricePerUnit(calculateSalesPrice(createProductDtoRequest.getSupplierPrice(),
          product.getProfitPercentage()));
    }
    if (createProductDtoRequest.getSuppliersAfm() != null) {
      Supplier supplier = supplierService
          .getSupplierByAfm(createProductDtoRequest.getSuppliersAfm());
      if (!product.getSuppliers()
          .stream()
          .map(Supplier::getId)
          .collect(Collectors.toSet())
          .contains(supplier.getId())) {
        Supplier newSupplier = supplierService
            .getSupplierByAfm(createProductDtoRequest.getSuppliersAfm());
        product.getSuppliers().add(newSupplier);
      }
    }

    if (createProductDtoRequest.getBarcode() != null) {
      if (!product.getBarcodes()
          .stream()
          .map(Barcode::getBarcode)
          .collect(Collectors.toSet())
          .contains(createProductDtoRequest.getBarcode())) {
        Barcode newBarcode = barcodeBuilder(createProductDtoRequest.getBarcode());
        newBarcode.setProductBarcode(product);
        barcodeService.createBarcode(newBarcode);

      }
    }
    product.setLastUpdated(OffsetDateTime.now());
    return productMapper.productToProductDto(productService.saveProduct(product));
  }

  @Override
  public Boolean deleteProductById(Long productId) {
    productService.deleteProductById(productId);
    return Boolean.TRUE;
  }

  @Override
  public List<ProductDtoForList> getAllProducts() {
    return productMapper.entityListToModel(productService.getAllProducts());
  }

  @Override
  public ProductDto getProductById(Long productId) {
    return productMapper.productToProductDto(productService.findProductById(productId));
  }

  @Override
  public ProductDto createLabelByProductId(Long productId) {
    Product product = productService.findProductById(productId);
    try {
      labelService.createLabelForProductByProduct(product);
    } catch (IOException e) {
      throw new LabelServiceException(LabelError.CANNOT_CREATE_LABEL);
    } catch (URISyntaxException e) {
      throw new LabelServiceException(LabelError.CANNOT_CREATE_LABEL);
    } catch (DocumentException e) {
      throw new LabelServiceException(LabelError.CANNOT_CREATE_LABEL);
    }
    return productMapper.productToProductDto(product);
  }

  @Override
  public List<ProductDtoForList> createLabelsByListOfProductIds(
      CreateLabelsByListOfProductIds createLabelsByListOfProductIds) {
    List<Product> products = productService
        .findAllById(createLabelsByListOfProductIds.getProductIds());
    try {
      labelService.createLabelsForProductsByAListOfProducts(products);
    } catch (IOException e) {
      throw new LabelServiceException(LabelError.CANNOT_CREATE_LABEL);
    } catch (URISyntaxException e) {
      throw new LabelServiceException(LabelError.CANNOT_CREATE_LABEL);
    } catch (DocumentException e) {
      throw new LabelServiceException(LabelError.CANNOT_CREATE_LABEL);
    }
    return productMapper.entityListToModel(products);
  }

  private Barcode barcodeBuilder(String barcodeString) {
    return Barcode.builder()
        .barcode(barcodeString)
        .build();
  }

  private BigDecimal calculateSalesPrice(BigDecimal suppliersPrice, BigDecimal profitPercentage) {
    return ((suppliersPrice.multiply(profitPercentage.divide(new BigDecimal(100))))
        .add(suppliersPrice)).multiply(Constants.FPA).setScale(2, BigDecimal.ROUND_UP);
  }

  private Category createCategoryRequestDtoBuilder(Long subCategoryId) {
    return categoryService.findCategoryById(subCategoryId);

  }

}
