package gr.darras.marketcafe.sales.business.service.product;

import gr.darras.marketcafe.sales.model.product.CreateLabelsByListOfProductIds;
import gr.darras.marketcafe.sales.model.product.CreateProductDtoRequest;
import gr.darras.marketcafe.sales.model.product.ProductDto;
import gr.darras.marketcafe.sales.model.product.ProductDtoForList;

import java.util.List;

public interface BusinessProductService {

    ProductDto createProduct(CreateProductDtoRequest createProductDtoRequest);

    ProductDto editProductById(CreateProductDtoRequest createProductDtoRequest, Long productId);

    Boolean deleteProductById(Long productId);

    List<ProductDtoForList> getAllProducts();

    ProductDto getProductById(Long productId);

    ProductDto createLabelByProductId(Long productId);

    List<ProductDtoForList> createLabelsByListOfProductIds(CreateLabelsByListOfProductIds createLabelsByListOfProductIds);

}
