package gr.darras.marketcafe.sales.business.service.category;

import gr.darras.marketcafe.sales.model.category.CategoryDtoAllResponse;
import gr.darras.marketcafe.sales.model.category.CategoryDtoParentResponse;
import gr.darras.marketcafe.sales.model.category.CreateCategoryRequestDto;
import java.util.List;

public interface BusinessCategoryService {

  CategoryDtoAllResponse saveCategory(CreateCategoryRequestDto createCategoryRequestDto);

  List<CategoryDtoParentResponse> getAllParentCategories();

  List<CategoryDtoAllResponse> getAllSubCategories(Long id);

}