package gr.darras.marketcafe.sales.business.service.barcode;

import gr.darras.marketcafe.sales.jpa.entity.Barcode;

public interface BusinessBarcodeService {

    Barcode createBarcode(Barcode barcode);
}
