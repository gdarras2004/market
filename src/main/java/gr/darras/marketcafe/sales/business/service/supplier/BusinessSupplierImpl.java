package gr.darras.marketcafe.sales.business.service.supplier;

import gr.darras.marketcafe.sales.jpa.entity.Supplier;
import gr.darras.marketcafe.sales.jpa.service.supplier.SupplierService;
import gr.darras.marketcafe.sales.mapper.SupplierMapper;
import gr.darras.marketcafe.sales.model.supplier.CreateSupplierRequestDto;
import gr.darras.marketcafe.sales.model.supplier.SupplierDto;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;

@Service
public class BusinessSupplierImpl implements BusinessSupplier {

    private final SupplierService supplierService;
    private final SupplierMapper supplierMapper;


    public BusinessSupplierImpl(
            SupplierService supplierService,
            SupplierMapper supplierMapper) {
        this.supplierService = supplierService;
        this.supplierMapper = supplierMapper;
    }

    @Override
    public SupplierDto createSupplier(CreateSupplierRequestDto createSupplierRequestDto) {
        Supplier supplier = Supplier.builder()
                .address(createSupplierRequestDto.getAddress())
                .companyName(createSupplierRequestDto.getCompanyName())
                .companyType(createSupplierRequestDto.getCompanyType())
                .creationDate(OffsetDateTime.now())
                .lastUpdated(OffsetDateTime.now())
                .afm(createSupplierRequestDto.getAfm())
                .phone1(createSupplierRequestDto.getPhone1())
                .phone2(createSupplierRequestDto.getPhone2())
                .email(createSupplierRequestDto.getEmail())
                .postCode(createSupplierRequestDto.getPostCode())
                .taxOffice(createSupplierRequestDto.getTaxOffice())
                .build();
        return supplierMapper.mapToModel(supplierService.saveSupplier(supplier));
    }

    @Override
    public SupplierDto updateSupplier(Long id, CreateSupplierRequestDto createSupplierRequestDto) {
        return null;
    }

    @Override
    public Boolean deleteSupplier(Long id) {
        supplierService.deleteSupplier(id);
        return Boolean.TRUE;
    }

    @Override
    public List<SupplierDto> getSuppliers() {
        return supplierMapper.mapToModelList(supplierService.getSuppliers());
    }

    @Override
    public List<SupplierDto> getSuppliersByDescription(String search) {
        return supplierMapper.mapToModelList(supplierService.getSuppliersByDescription(search));
    }

    @Override
    public SupplierDto findSupplierByAfm(Long afm) {
        return null;
    }
}
