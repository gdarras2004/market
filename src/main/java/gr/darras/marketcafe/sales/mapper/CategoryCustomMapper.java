package gr.darras.marketcafe.sales.mapper;

import gr.darras.marketcafe.sales.jpa.entity.Category;
import gr.darras.marketcafe.sales.model.category.CategoryDtoAllResponse;
import gr.darras.marketcafe.sales.model.category.CategoryDtoParentResponse;
import java.util.List;

public interface CategoryCustomMapper {


  List<CategoryDtoAllResponse> entityListToModel(List<Category> categories);

  List<CategoryDtoParentResponse> entityListToModelParent(List<Category> categories);

  CategoryDtoAllResponse entityToModel(Category category);

}
