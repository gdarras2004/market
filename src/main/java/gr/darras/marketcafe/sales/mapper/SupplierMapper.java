package gr.darras.marketcafe.sales.mapper;

import gr.darras.marketcafe.sales.jpa.entity.Supplier;
import gr.darras.marketcafe.sales.model.supplier.SupplierDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SupplierMapper {

    SupplierMapper INSTANCE = Mappers.getMapper(SupplierMapper.class);

    SupplierDto mapToModel(Supplier supplier);

    List<SupplierDto> mapToModelList(List<Supplier> suppliers);

    Supplier mapToEntity(SupplierDto supplierDto);

}
