package gr.darras.marketcafe.sales.mapper;

import gr.darras.marketcafe.sales.jpa.entity.Category;
import gr.darras.marketcafe.sales.jpa.entity.Product;
import gr.darras.marketcafe.sales.model.category.CategoryDtoAllResponse;
import gr.darras.marketcafe.sales.model.category.CategoryDtoParentResponse;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryCustomMapperImpl implements CategoryCustomMapper {
    @Override
    public List<CategoryDtoAllResponse> entityListToModel(List<Category> categories) {
        List<CategoryDtoAllResponse> categoryDtoAllResponseList = new ArrayList<>();
        categories.stream().forEach(category -> {
            categoryDtoAllResponseList.add(CategoryDtoAllResponse.builder()
                    .categoryId(category.getId())
                    .categoryName(category.getName())
                    .categoryParentName(category.getParent().getName())
                    .productIds(checkIfCategoryHasProducts(category) ? category.getProduct().stream().map(Product::getId).collect(Collectors.toSet()) : Collections.emptySet())
                    .build());
        });

        return categoryDtoAllResponseList;
    }

    @Override
    public List<CategoryDtoParentResponse> entityListToModelParent(List<Category> categories) {
        List<CategoryDtoParentResponse> categoryDtoParentResponseArrayList = new ArrayList<>();
        categories.stream().forEach(category -> {
            categoryDtoParentResponseArrayList.add(CategoryDtoParentResponse.builder()
                    .categoryId(category.getId())
                    .categoryName(category.getName())
                    .build());
        });
        return categoryDtoParentResponseArrayList;
    }

    @Override
    public CategoryDtoAllResponse entityToModel(Category category) {
        return CategoryDtoAllResponse.builder()
                .categoryId(category.getId())
                .categoryName(category.getName())
                .categoryParentName(checkIfCategoryHasParent(category) ? category.getParent().getName() : null)
                .productIds(checkIfCategoryHasProducts(category) ? category.getProduct().stream().map(Product::getId).collect(Collectors.toSet()) : Collections.emptySet())
                .build();
    }

    private Boolean checkIfCategoryHasParent(Category category) {
        if (category.getParent() != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private Boolean checkIfCategoryHasProducts(Category category) {
        if (category.getProduct() != null && !category.getProduct().isEmpty()) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
