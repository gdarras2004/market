package gr.darras.marketcafe.sales.mapper;

import gr.darras.marketcafe.sales.jpa.entity.Barcode;
import gr.darras.marketcafe.sales.model.BarcodeDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface BarcodeMapper {

    BarcodeMapper INSTANCE = Mappers.getMapper(BarcodeMapper.class);

    BarcodeDto barcodeToBarcodeDto(Barcode barcode);

}
