package gr.darras.marketcafe.sales.mapper;

import gr.darras.marketcafe.sales.jpa.entity.Product;
import gr.darras.marketcafe.sales.jpa.entity.Supplier;
import gr.darras.marketcafe.sales.model.product.CreateProductDtoResponse;
import gr.darras.marketcafe.sales.model.product.ProductDto;
import gr.darras.marketcafe.sales.model.product.ProductDtoForList;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    ProductDto productToProductDto(Product product);

    List<ProductDtoForList> entityListToModel(List<Product> products);
}
