package gr.darras.marketcafe.sales.jpa.service.supplier;

import gr.darras.marketcafe.sales.error.JpaError;
import gr.darras.marketcafe.sales.exception.JpaException;
import gr.darras.marketcafe.sales.jpa.entity.Supplier;
import gr.darras.marketcafe.sales.jpa.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {

    private final SupplierRepository supplierRepository;

    @Autowired
    public SupplierServiceImpl(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    @Override
    public Supplier getSupplierByAfm(String afm) {
        return supplierRepository.findByAfm(afm);
    }

    @Override
    public Supplier saveSupplier(Supplier supplier) {
        return supplierRepository.save(supplier);
    }

    @Override
    public void deleteSupplier(Long id) {
        supplierRepository.deleteById(id);
    }

    @Override
    @Transactional
    public Supplier getSupplierById(Long id) {
        return supplierRepository.findById(id).orElseThrow(() -> new JpaException(JpaError.ENTITY_NOT_FOUND));
    }

    @Override
    public List<Supplier> getSuppliers() {
        return supplierRepository.findAll();
    }

    @Override
    public List<Supplier> getSuppliersByDescription(String search) {
        return supplierRepository.findByCompanyNameContainsIgnoreCaseOrAfmContainsIgnoreCase(search, search);
    }
}
