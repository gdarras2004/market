package gr.darras.marketcafe.sales.jpa.service.category;

import gr.darras.marketcafe.sales.error.JpaError;
import gr.darras.marketcafe.sales.exception.JpaException;
import gr.darras.marketcafe.sales.jpa.entity.Category;
import gr.darras.marketcafe.sales.jpa.repository.CategoryRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryServiceImpl implements CategoryService {

  private final CategoryRepository categoryRepository;

  public CategoryServiceImpl(
      CategoryRepository categoryRepository) {
    this.categoryRepository = categoryRepository;
  }

  @Override
  @Transactional
  public Category saveCategory(Category category) {
    return categoryRepository.save(category);
  }


  @Override
  public Category findCategoryByName(String categoryName) {
    return Optional.ofNullable(categoryRepository.findByName(categoryName))
        .orElseThrow(() -> new JpaException(
            JpaError.ENTITY_NOT_FOUND));
  }

  @Override
  public Category findCategoryById(Long id) {
    return categoryRepository.findById(id)
        .orElseThrow(() -> new JpaException(JpaError.ENTITY_NOT_FOUND));
  }

  @Override
  public List<Category> getCategory() {
    return categoryRepository.findAll();
  }

  @Override
  public List<Category> getAllParentCategories() {
    return categoryRepository.findAllByParentIsNullOrderByName();
  }

  @Override
  public List<Category> getAllSubCategories(Long id) {
    return categoryRepository.findAllByParent_Id(id);
  }


}
