package gr.darras.marketcafe.sales.jpa.repository;

import gr.darras.marketcafe.sales.jpa.entity.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {

    Supplier findByAfm(String afm);

    List<Supplier> findByCompanyNameContainsIgnoreCaseOrAfmContainsIgnoreCase(String searchCompanyName, String searchAfm);
}
