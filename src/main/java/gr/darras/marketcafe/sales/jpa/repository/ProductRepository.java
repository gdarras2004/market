package gr.darras.marketcafe.sales.jpa.repository;

import gr.darras.marketcafe.sales.jpa.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}
