package gr.darras.marketcafe.sales.jpa.repository;

import gr.darras.marketcafe.sales.jpa.entity.Barcode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BarcodeRepository extends JpaRepository<Barcode, Long> {
}
