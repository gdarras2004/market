package gr.darras.marketcafe.sales.jpa.entity;

import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.CascadeType.*;
import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "SUPPLIER", schema = "sales")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@ToString(exclude = {"suppliersProducts"})
public class Supplier {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "supplier_id_seq")
    @SequenceGenerator(name = "supplier_id_seq", sequenceName = "supplier_id_seq", schema = "sales", allocationSize = 1)
    private Long id;

    @Column(name = "AFM", unique = true, length = 50)
    private String afm;

    @Column(name = "COMPANY_NAME", nullable = false, length = 50)
    private String companyName;

    @Column(name = "COMPANY_TYPE", nullable = false, length = 50)
    private String companyType;

    @Column(name = "TAX_OFFICE", nullable = false, length = 50)
    private String taxOffice;

    @Column(name = "ADDRESS", nullable = false, length = 255)
    private String address;

    @Column(name = "EMAIL", length = 50)
    private String email;

    @Column(name = "POST_CODE")
    private Long postCode;

    @Column(name = "PHONE1")
    private Long phone1;

    @Column(name = "PHONE2")
    private Long phone2;

    @Column(name = "CREATION_DATE", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime creationDate;

    @Column(name = "LAST_UPDATED", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime lastUpdated;

    @OneToMany(fetch = LAZY, cascade = {MERGE, PERSIST, REFRESH, DETACH})
    @JoinTable(
            name = "SUPPLIER_PRODUCT", schema = "sales",
            joinColumns = {
                    @JoinColumn(name = "SUPPLIER_ID",
                            foreignKey = @ForeignKey(name = "FK_SUPPLIER"), nullable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "PRODUCT_ID",
                            foreignKey = @ForeignKey(name = "FK_PRODUCT"), nullable = false)})
    private Set<Product> suppliersProducts;

    public Set<Product> getProducts(){
        if (suppliersProducts == null){
            suppliersProducts = new HashSet<>();
        }
        return suppliersProducts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Supplier supplier = (Supplier) o;
        return afm.equals(supplier.afm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(afm);
    }
}
