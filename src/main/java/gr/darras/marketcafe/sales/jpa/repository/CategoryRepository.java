package gr.darras.marketcafe.sales.jpa.repository;

import gr.darras.marketcafe.sales.jpa.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

  Category findByName(String ParentName);

  List<Category> findAllByParentIsNullOrderByName();

  List<Category> findAllByParent_Id(Long id);

}
