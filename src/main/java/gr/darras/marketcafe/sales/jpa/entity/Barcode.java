package gr.darras.marketcafe.sales.jpa.entity;

import lombok.*;

import javax.persistence.*;

import java.util.Set;

import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "BARCODE", schema = "sales")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@ToString
public class Barcode {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "barcode_id_seq")
    @SequenceGenerator(name = "barcode_id_seq", sequenceName = "barcode_id_seq", schema = "sales", allocationSize = 1)
    private Long id;

    @Column(name = "BARCODE", nullable = false, length = 50)
    private String barcode;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {MERGE, PERSIST, REFRESH, DETACH})
    @JoinColumn(name = "PRODUCT_ID",
            referencedColumnName = "ID",
            foreignKey = @ForeignKey(name = "FK_BARCODE_PRODUCT"),
            nullable = false)
    private Product productBarcode;

}
