package gr.darras.marketcafe.sales.jpa.service.pdf;

import com.itextpdf.text.DocumentException;
import gr.darras.marketcafe.sales.jpa.entity.Product;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface LabelService {

    void createLabelForProductByProduct(Product product) throws IOException, DocumentException, URISyntaxException;

    void createLabelsForProductsByAListOfProducts(List<Product> products) throws IOException, DocumentException, URISyntaxException;
}
