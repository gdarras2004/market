package gr.darras.marketcafe.sales.jpa.service.supplier;

import gr.darras.marketcafe.sales.jpa.entity.Supplier;

import java.util.List;

public interface SupplierService {

    Supplier getSupplierByAfm(String afm);

    Supplier saveSupplier(Supplier supplier);

    void deleteSupplier(Long id);

    Supplier getSupplierById(Long id);

    List<Supplier> getSuppliers();

    List<Supplier> getSuppliersByDescription(String search);

}
