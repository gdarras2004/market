package gr.darras.marketcafe.sales.jpa.service.category;

import gr.darras.marketcafe.sales.jpa.entity.Category;
import java.util.List;

public interface CategoryService {

  Category saveCategory(Category category);

  Category findCategoryByName(String categoryName);

  Category findCategoryById(Long id);

  List<Category> getCategory ();

  List<Category> getAllParentCategories ();

  List<Category> getAllSubCategories (Long id);

}
