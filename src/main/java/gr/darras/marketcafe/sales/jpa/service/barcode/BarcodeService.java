package gr.darras.marketcafe.sales.jpa.service.barcode;

import gr.darras.marketcafe.sales.jpa.entity.Barcode;

public interface BarcodeService {

    Barcode createBarcode(Barcode barcode);
}
