package gr.darras.marketcafe.sales.jpa.service.barcode;

import gr.darras.marketcafe.sales.jpa.entity.Barcode;
import gr.darras.marketcafe.sales.jpa.repository.BarcodeRepository;
import org.springframework.stereotype.Service;

@Service
public class BarcodeServiceImpl implements BarcodeService {

    private final BarcodeRepository barcodeRepository;

    public BarcodeServiceImpl(BarcodeRepository barcodeRepository) {
        this.barcodeRepository = barcodeRepository;
    }

    @Override
    public Barcode createBarcode(Barcode barcode) {
        return barcodeRepository.save(barcode);
    }
}
