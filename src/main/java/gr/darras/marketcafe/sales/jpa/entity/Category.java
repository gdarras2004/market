package gr.darras.marketcafe.sales.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "CATEGORY", schema = "sales")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@JsonIgnoreProperties({"children"})
public class Category {

  @Id
  @Column(name = "ID", nullable = false)
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "category_id_seq")
  @SequenceGenerator(name = "category_id_seq", sequenceName = "category_id_seq", schema = "sales", allocationSize = 1)
  private Long id;

  @Column(name = "NAME", nullable = false, length = 128)
  private String name;

  @OneToOne
  @JoinColumn(name = "PARENT_ID")
  private Category parent;

  @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
  private Set<Category> children = new HashSet<>();

  @OneToMany(mappedBy = "category")
  private Set<Product> product = new HashSet<>();


  public void addChild(Category children) {
    this.children.add(children);
  }

  public void addProduct(Product product) {
    this.product.add(product);
  }

  @Override
  public String toString() {
    return "Category{" +
        "parent=" + parent +
        ", children=" + children +
        '}';
  }
}
