package gr.darras.marketcafe.sales.jpa.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.CascadeType.*;
import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "PRODUCT", schema = "sales")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Product {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "product_id_seq")
    @SequenceGenerator(name = "product_id_seq", sequenceName = "product_id_seq", schema = "sales", allocationSize = 1)
    private Long id;

    @Column(name = "QUANTITY", nullable = false)
    private Long quantity;

    @Column(name = "DESCRIPTION_EN ", nullable = false, length = 255)
    private String descriptionEn;

    @Column(name = "DESCRIPTION_GR ", nullable = false, length = 255)
    private String descriptionGr;

    @Column(name = "SUPPLIER_PRICE", nullable = false)
    private BigDecimal supplierPricePerUnit;

    @Column(name = "SALES_PRICE", nullable = false)
    private BigDecimal salesPricePerUnit;

    @Column(name = "PROFIT_PERCENTAGE", nullable = false)
    private BigDecimal profitPercentage;

    @Column(name = "CREATION_DATE", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime creationDate;

    @Column(name = "LAST_UPDATED", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime lastUpdated;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH, DETACH})
    @JoinColumn(name = "CATEGORY_ID", referencedColumnName = "id")
    private Category category;

    @OneToMany(fetch = LAZY, cascade = {MERGE, PERSIST, REFRESH, DETACH})
    @JoinTable(
            name = "SUPPLIER_PRODUCT", schema = "sales",
            joinColumns = {
                    @JoinColumn(name = "PRODUCT_ID",
                            foreignKey = @ForeignKey(name = "FK_PRODUCT"), nullable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "SUPPLIER_ID",
                            foreignKey = @ForeignKey(name = "FK_SUPPLIER"), nullable = false)})
    private Set<Supplier> suppliers;

    @OneToMany(mappedBy = "productBarcode", fetch = LAZY)
    private Set<Barcode> productBarcodes;

    public Set<Barcode> getBarcodes(){
        if (productBarcodes == null){
            productBarcodes = new HashSet<>();
        }
        return productBarcodes;
    }

    public Set<Supplier> getSuppliers(){
        if (suppliers == null){
            suppliers = new HashSet<>();
        }
        return suppliers;
    }
}
