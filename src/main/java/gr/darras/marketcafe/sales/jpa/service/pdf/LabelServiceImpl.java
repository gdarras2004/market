package gr.darras.marketcafe.sales.jpa.service.pdf;

import static gr.darras.marketcafe.sales.common.Constants.BASE_PATH;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import gr.darras.marketcafe.sales.jpa.entity.Product;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LabelServiceImpl implements LabelService {

  final static Logger logger = LoggerFactory.getLogger(LabelServiceImpl.class);

  @Override
  public void createLabelForProductByProduct(Product product)
      throws IOException, DocumentException, URISyntaxException {
    logger.debug("Start to create Label.... please Wait");
    Document document = new Document();
    PdfWriter.getInstance(document,
        new FileOutputStream(BASE_PATH +"/labels/Label_" + product.getDescriptionGr() + ".pdf"));

    document.open();
    PdfPTable table = getPdfPTable(product);
    document.add(table);
    document.close();
    logger.debug("===============>Finish");
  }

  @Override
  public void createLabelsForProductsByAListOfProducts(List<Product> products)
      throws IOException, DocumentException, URISyntaxException {
    logger.debug("Start to create Labels.... please Wait");
    Document document = new Document();
    PdfWriter.getInstance(document,
        new FileOutputStream(BASE_PATH + "/labels/List_of_Labels_size_" + products.size() + ".pdf"));

    document.open();
    for (Product p : products) {
      PdfPTable table = getPdfPTable(p);
      document.add(table);
    }
    document.close();
    logger.debug("===============>Finish");
  }

  private PdfPTable getPdfPTable(Product product)
      throws URISyntaxException, DocumentException, IOException {
    PdfPTable table = new PdfPTable(new float[]{1, 10, 10});
    table.setWidthPercentage(50);

//       GREEKs
//    String url = "C:/Windows/Fonts/Arial.ttf";
    String url = "fonts/NotoSans-Regular.ttf";
    int size = 12;

    BaseFont fonty = BaseFont.createFont(url,  BaseFont.IDENTITY_H, true);
    Font greekFont = new Font(fonty, size, Font.NORMAL);



    table.setHorizontalAlignment(Element.ALIGN_LEFT);

    //Product DescriptionGr
    Font fontForDescriptionGr = new Font(greekFont);
    Phrase descriptionPhraseGr = new Phrase(product.getDescriptionGr(), fontForDescriptionGr);
    PdfPCell descriptionCellGr = pdfCellBuilder(BaseColor.LIGHT_GRAY, Element.ALIGN_CENTER,
        Element.ALIGN_CENTER, PdfPCell.TOP, 3, 20, descriptionPhraseGr);
    table.addCell(descriptionCellGr);

    //Product DescriptionEn
    Font fontForDescriptionEn = new Font(greekFont);
    Phrase descriptionPhraseEn = new Phrase(product.getDescriptionEn(), fontForDescriptionEn);
    PdfPCell descriptionCellEn = pdfCellBuilder(BaseColor.LIGHT_GRAY, Element.ALIGN_CENTER,
        Element.ALIGN_CENTER, PdfPCell.ALIGN_LEFT, 3, 20, descriptionPhraseEn);
    table.addCell(descriptionCellEn);

    //Logo for Label
    Path path = Paths.get(BASE_PATH + "/imgs/DarrasLogo.png");
    Image img = Image.getInstance(path.toAbsolutePath().toString());
    img.scalePercent(20);
    PdfPCell logoForLabelCell = new PdfPCell(img);
    logoForLabelCell.setFixedHeight(10);
    logoForLabelCell.setColspan(1);
    logoForLabelCell.setBorder(2);
    table.addCell(logoForLabelCell);

    //Title "price" for Label
    Font fontForPriceTitle = new Font(greekFont);
    Phrase pricePhrase = new Phrase("Price/Τιμή: ", fontForPriceTitle);
    PdfPCell priceCell = pdfCellBuilder(BaseColor.WHITE, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,
        2, 0, 40, pricePhrase);
    table.addCell(priceCell);

    //The actual product price for Label
    Font fontH1 = new Font(Font.FontFamily.COURIER, 25, Font.BOLD);
    Phrase productPricePhrase = new Phrase(product.getSalesPricePerUnit() + " €", fontH1);
    PdfPCell productPriceCell = pdfCellBuilder(BaseColor.WHITE, Element.ALIGN_CENTER, Element.ALIGN_CENTER,
        15, 0, 40, productPricePhrase );
    table.addCell(productPriceCell);

    Font fontForProductId = new Font(Font.FontFamily.COURIER, 5);
    Phrase productIdPhrase = new Phrase("Product Code: " + product.getId(), fontForProductId);
    PdfPCell productId = pdfCellBuilder(BaseColor.LIGHT_GRAY, Element.ALIGN_CENTER, Element.ALIGN_CENTER, PdfPCell.BOTTOM,
        3, 10, productIdPhrase);
    table.addCell(productId);

    return table;
  }

  private PdfPCell pdfCellBuilder(BaseColor backgroundColor, int horizontalAlignment,
      int verticalAlignment, int border, int colspan, float fixedHeight, Phrase cellPhrase) {
    PdfPCell pdfPCell = new PdfPCell(cellPhrase);
    pdfPCell.setBackgroundColor(backgroundColor);
    pdfPCell.setHorizontalAlignment(horizontalAlignment);
    pdfPCell.setVerticalAlignment(verticalAlignment);
    pdfPCell.setBorder(border);
    pdfPCell.setColspan(colspan);
    pdfPCell.setFixedHeight(fixedHeight);
    return pdfPCell;



  }


}
