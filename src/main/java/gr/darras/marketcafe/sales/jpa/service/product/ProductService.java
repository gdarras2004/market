package gr.darras.marketcafe.sales.jpa.service.product;

import gr.darras.marketcafe.sales.jpa.entity.Product;

import java.util.List;

public interface ProductService {

    Product saveProduct(Product product);

    Product findProductById(Long id);

    List<Product> getAllProducts();

    List<Product> findAllById(List<Long> ids);

    void deleteProductById(Long productId);
}
